#ifndef UTILS_H
#define UTILS_H

#include <QMap>
#include <QString>
#include <QFileInfo>

class utils
{
    public:
        static QString getFileSizeString(const QString &file);
        static QList<QString> convertSize(qint64 size);

    private:
        utils();
};

#endif // UTILS_H
