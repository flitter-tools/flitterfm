#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>
#include <QMap>
#include <QSize>

#include "third-party/ftlip/QT/ftlip.h"

class Settings
{
    public:
        Settings();
        ~Settings();

        enum MODE
        {
            ICONS = 0,
            LIST,
            TC
        };

        void setPreviewCache(const QString &file, const QString &seed);

        void setFolderColor(const QString &dir, const int &color);
        QMap<QString, int> getFolderColors() const;

        void setMimeCache(const QString &filetype, const QString &programm);

        void saveUrl(const QString &url);
        void saveIconSize(const int &size);
        void saveMode(const int &mode);
        void saveWindowSize(const int &wight, const int &height);
        void saveUnwrap(const bool &unwrap);
        void saveSplitter(const QByteArray &state);

        QString loadUrl() const;
        int loadIconSize() const;
        int loadMode() const;
        QSize loadWindowSize() const;
        bool loadUnwrap() const;
        QByteArray loadSplitter() const;

    private:
        ftlip *app_settings;
        ftlip *preview_cache;
        ftlip *mime_cache;
};

#endif // SETTINGS_H
