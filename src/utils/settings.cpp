#include "settings.h"

#include <QDir>
#include <QDebug>
#include <QString>
#include <QSettings>
#include <QMimeDatabase>

Settings::Settings()
{
    const QString &home = QDir::homePath();
    app_settings = new ftlip(home + "/.config/flitter/fm/settings.cfg");
    mime_cache = new ftlip(home + "/.local/share/applications/mimeapps.list");
    preview_cache = new ftlip(home + "/.cache/flitter/previewdb.cfg");
}

Settings::~Settings()
{
    delete app_settings;
    delete mime_cache;
    delete preview_cache;
}

void Settings::setPreviewCache(const QString &file, const QString &seed)
{
    preview_cache->set(file, seed);
}

void Settings::setFolderColor(const QString &dir, const int &color)
{
    app_settings->set(dir, QString::number(color));
}

QMap<QString, int> Settings::getFolderColors() const
{
    QMap<QString, int> ret;

    QFile settings(QDir::homePath() + "/.config/flitter/fm/settings.cfg");
    if (settings.open(QIODevice::ReadOnly))
    {
       QTextStream in(&settings);

       while (!in.atEnd())
       {
           QString line = in.readLine();

           if (line[0] == '/')
           {
                QStringList path = line.split("=");
                ret.insert(path[0], path[1].toInt());
           }
       }

       settings.close();
    }

    return ret;
}

void Settings::setMimeCache(const QString &filetype, const QString &programm)
{
    const QString &s = mime_cache->get(filetype);
    const QString &ft = QMimeDatabase().mimeTypeForFile(filetype).name();
    mime_cache->setSection("Added Associations", ft, programm + ";" + s);
}

void Settings::saveUrl(const QString &url)
{
    app_settings->set("LastUrl", url);
}

void Settings::saveIconSize(const int &size)
{
    app_settings->set("LastIconSize", QString::number(size));
}

void Settings::saveMode(const int &mode)
{
    app_settings->set("Mode", QString::number(mode));
}

void Settings::saveWindowSize(const int &wight, const int &height)
{
    app_settings->set("WindowSize", QString::number(wight) + "x" + QString::number(height));
}

void Settings::saveUnwrap(const bool &unwrap)
{
    app_settings->set("Unwrap", unwrap ? "true" : "false");
}

void Settings::saveSplitter(const QByteArray &state)
{
    app_settings->set("Splitter", state);
}

QString Settings::loadUrl() const
{
    QString url = app_settings->get("LastUrl");
    if (!url.isEmpty())
        return url;

    return QDir::homePath();
}

int Settings::loadIconSize() const
{
    QString size = app_settings->get("LastIconSize");
    if (!size.isEmpty())
        return size.toInt();

    return 64;
}

int Settings::loadMode() const
{
    return app_settings->get("Mode").toInt();
}

QSize Settings::loadWindowSize() const
{
    QString size = app_settings->get("WindowSize");

    if (!size.isEmpty())
    {
        int width = size.split("x")[0].toInt();
        int height = size.split("x")[1].toInt();
        return QSize(width, height);
    }

    return QSize(800, 600);
}

bool Settings::loadUnwrap() const
{
    QString unwrap = app_settings->get("Unwrap");

    if (unwrap == "true")
        return true;
    else
        return false;
}

QByteArray Settings::loadSplitter() const
{
    const QString &tmp = app_settings->get("Splitter");
    if (tmp.isEmpty())
    {
        QByteArray ret(tmp.toLatin1(), tmp.length());
        return ret;
    }

    return QByteArray();
}
