#ifndef MOUNTPOINTS_H
#define MOUNTPOINTS_H

#include <QMap>
#include <QString>
#include <QIcon>

class MountPoints
{
    public:
        MountPoints();

        struct DEVICES
        {
            QString name;
            QIcon icon;
        };

        QList<DEVICES> devices();
        static void mount(const QString &point);
        static void umount(const QString &point);
        static void mountftp(const QString &url, const QString &user, const QString &password);
        static QString deviceProperties(const QString &url);
};

#endif // MOUNTPOINTS_H
