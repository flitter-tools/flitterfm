#include "mountpoints.h"

#include <QFile>
#include <QTextStream>
#include <QProcess>
#include <QDir>
#include <QDebug>

MountPoints::MountPoints(){}

QList<MountPoints::DEVICES> MountPoints::devices()
{
    QFile file("/etc/mtab");
    if (!file.open(QFile::ReadOnly))
        return {};

    QTextStream mtab(&file);

    QStringList mounts = mtab.readAll().split("\n");

    QList<MountPoints::DEVICES> ret;

    for (const QString &item : mounts)
    {
        if (item.isEmpty())
            continue;

        // ignore pseudo filesystem
        QStringList current = item.split(" ");
        if (current[0][0] != "/")
            continue;

        MountPoints::DEVICES dev;

        dev.name = current[1];

        if (current[2].contains("ext")) // probably is drive
            dev.icon = QIcon::fromTheme("cache");
        else if (current[2] == "iso9660") // iso file
            dev.icon = QIcon::fromTheme("tools-media-optical-format");
        else if (QString(current[1]).contains("ftp", Qt::CaseInsensitive)) // ftp mount
            dev.icon = QIcon::fromTheme("network-connect");
        else // usb stick
            dev.icon = QIcon::fromTheme("dialog-input-devices");

        ret.push_back(dev);
    }

    return ret;
}

QString MountPoints::deviceProperties(const QString &url)
{
    QFile file("/etc/mtab");
    if (!file.open(QFile::ReadOnly))
        return {};

    QTextStream mtab(&file);

    QStringList mounts = mtab.readAll().split("\n");

    for (const QString &item : mounts)
    {
        if (item.isEmpty())
            continue;

        if (item.split(" ")[1] == url)
            return item;
    }

    return "";
}

void MountPoints::mount(const QString &point)
{
    QProcess proc;
    proc.start("udevil mount " + point);
}

void MountPoints::umount(const QString &point)
{
    QProcess proc;
    proc.start("udevil umount " + point);
}

void MountPoints::mountftp(const QString &url, const QString &user, const QString &password)
{
    QDir().mkpath(QDir::homePath() + "/.cache/flitter/fm/" + url);

    QProcess *proc = new QProcess;
    proc->start("curlftpfs " + user + ":" + password + "@" + url + " " + QDir::homePath() + "/.cache/flitter/fm/" + url);
    proc->waitForFinished();
}
