#ifndef MIME_H
#define MIME_H

#include <QString>
#include <QIcon>
#include <QPair>
#include <QStringList>

#include <libffmpegthumbnailer/videothumbnailer.h>
#include <libffmpegthumbnailer/filmstripfilter.h>

/*FFMpeg thumbnailer code was taken from here:
 * https://github.com/dirkvdb/ffmpegthumbnailer/blob/master/kffmpegthumbnailer/kffmpegthumbnailer.cpp*/

class Mime
{
    public:
        Mime();
        virtual ~Mime(){}

        struct Associations
        {
            QIcon icon;
            QString exec;
            QString name;
            QString desktop;
        };

        static QString getProgramm(const QString &file);
        static QList<Mime::Associations> getAssociations(const QString &file);
        QMap<QString, QString> getXDGDirs();
        QIcon getMimeIcon(const QString &url, const QString &file);
        QIcon getFolderIcon(const QString &url, const QString &file);

    private:
        ffmpegthumbnailer::VideoThumbnailer m_Thumbnailer;
        ffmpegthumbnailer::FilmStripFilter m_FilmStrip;

        bool generatePreview(const QString &file);
        void generateVideoPreview(const QString &file);
};

#endif // MIME_H
