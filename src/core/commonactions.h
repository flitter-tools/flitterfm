#ifndef COMMONACTIONS_H
#define COMMONACTIONS_H

#include <QString>

#include "ui/newobjectdialog.h"

class CommonActions
{
    public:
        static bool rename(const QString &url, const QString &oldname, QString &newname);
        static bool move(const QStringList &oldpaths, const QString &newpath);
        static bool remove(const QStringList &paths);
        static bool copy(const QStringList &paths, const QString &newpath);
        static bool newObj(const QString &url, int mode);

    private:
        CommonActions();
};

#endif // COMMONACTIONS_H
