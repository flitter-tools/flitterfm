#include "commonactions.h"

#include <QMessageBox>
#include <QDir>
#include <QFileInfo>
#include <QFile>
#include <QInputDialog>
#include <QDebug>

bool CommonActions::copy(const QStringList &paths, const QString &newpath)
{

}

bool CommonActions::move(const QStringList &oldpaths, const QString &newpath)
{

}

bool CommonActions::newObj(const QString &url, int mode)
{
    NewObjectDialog dial(nullptr, url, mode);
    if (dial.exec() == QDialog::Accepted)
        return true;

    return false;
}

bool CommonActions::remove(const QStringList &paths)
{
    if (!paths.isEmpty())
    {
        QMessageBox *dial = new QMessageBox;
        dial->setModal(true);
        dial->setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        dial->setText("Delete " + QString::number(paths.count()) + " object(s)?");

        if (dial->exec() == QMessageBox::Ok)
        {
            for (const QString &item : paths)
            {
                QFileInfo fi(item);

                if (fi.isDir())
                {
                    QDir dir(item);
                    dir.removeRecursively();
                }
                else
                {
                    QFile file(item);
                    file.remove();
                }
            }
        }

        return true;
    }

    return false;
}

bool CommonActions::rename(const QString &url, const QString &oldname, QString &newname)
{
    QInputDialog *dial = new QInputDialog;
    dial->setModal(true);
    dial->setTextValue(oldname);
    dial->setLabelText("Edit file name:");

    if (dial->exec() == QInputDialog::Accepted)
    {
        QFile::rename(url + "/" + oldname, url + "/" + dial->textValue());

        newname = dial->textValue();

        return true;
    }

    return false;
}
