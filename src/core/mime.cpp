#include "mime.h"

#include <QSettings>
#include <QMimeDatabase>
#include <QPixmap>
#include <QImage>
#include <QPainter>
#include <QFile>
#include <QFileInfo>
#include <QDir>

#include <QDebug>

#include <iostream>
#include <vector>

#include <unistd.h>
#include <sys/syscall.h>
#include <linux/random.h>

#include "third-party/ftlip/QT/ftlip.h"
#include "utils/settings.h"

Mime::Mime()
{
    m_Thumbnailer.addFilter(&m_FilmStrip);
}

// https://wiki.archlinux.org/index.php/XDG_user_directories
QMap<QString, QString> Mime::getXDGDirs()
{
    QMap<QString, QString> ret;

    /*
     * I'm use ftlip here and bottom becasue QSettings can't realy parse mime files, because of,
     * for example: QSettings think ; is commentary, also user-dirs.dirs doesn't parse at all for
     * unknown reason
     *
     * TODO: write wrapper for ftlip using Qt strings
    */
    ftlip dirs(QDir::homePath() + "/.config/user-dirs.dirs");

    QString desktop = dirs.get("XDG_DESKTOP_DIR").remove("\"");
    QString download = dirs.get("XDG_DOWNLOAD_DIR").remove("\"");
    QString templates = dirs.get("XDG_TEMPLATES_DIR").remove("\"");
    QString publicshare = dirs.get("XDG_PUBLICSHARE_DIR").remove("\"");
    QString documents = dirs.get("XDG_DOCUMENTS_DIR").remove("\"");
    QString music = dirs.get("XDG_MUSIC_DIR").remove("\"");
    QString pictures = dirs.get("XDG_PICTURES_DIR").remove("\"");
    QString videos = dirs.get("XDG_VIDEOS_DIR").remove("\"");

    ret.insert("XDG_DESKTOP_DIR", desktop.replace("$HOME", QDir::homePath()));
    ret.insert("XDG_DOWNLOAD_DIR", download.replace("$HOME", QDir::homePath()));
    ret.insert("XDG_TEMPLATES_DIR", templates.replace("$HOME", QDir::homePath()));
    ret.insert("XDG_PUBLICSHARE_DIR", publicshare.replace("$HOME", QDir::homePath()));
    ret.insert("XDG_DOCUMENTS_DIR", documents.replace("$HOME", QDir::homePath()));
    ret.insert("XDG_MUSIC_DIR", music.replace("$HOME", QDir::homePath()));
    ret.insert("XDG_PICTURES_DIR", pictures.replace("$HOME", QDir::homePath()));
    ret.insert("XDG_VIDEOS_DIR", videos.replace("$HOME", QDir::homePath()));

    return ret;
}

QList<Mime::Associations> Mime::getAssociations(const QString &file)
{
    QList<Mime::Associations> ret;

    QString mime = QMimeDatabase().mimeTypeForFile(file).name();

    // check all mimes cache and concentrate in one string
    const QString &home = QDir::homePath();
    ftlip settingsmime(home + "/.local/share/applications/mimeapps.list");
    ftlip settingsdefault(home + "/.local/share/applications/defaults.list");
    ftlip settingsinfo(home + "/.local/share/applications/mimeinfo.cache");
    ftlip settingsdist("/usr/share/applications/mimeinfo.cache");

    QString desktop = settingsmime.get(mime) + settingsdefault.get(mime) + \
            settingsinfo.get(mime) + settingsdist.get(mime);

    for (const QString &item : desktop.split(";"))
    {
        if (item.isEmpty()) // last item also contains ; so it will be empty
            continue;

        // parse *.desktop file
        Mime::Associations assoc;
        QSettings desksett("/usr/share/applications/" + item, QSettings::IniFormat);
        desksett.beginGroup("Desktop Entry");
        assoc.exec = desksett.value("Exec").toString().split(" ")[0];
        assoc.icon = QIcon::fromTheme(desksett.value("Icon").toString());
        assoc.name = desksett.value("Name").toString();
        assoc.desktop = item;
        desksett.endGroup();

        ret.push_back(assoc);
    }

    return ret;
}

QString Mime::getProgramm(const QString &file)
{
    QFile defaultlist(QDir::homePath() + "/.local/share/applications/mimeapps.list");
    QString mime = QMimeDatabase().mimeTypeForFile(file).name();
    QString desktop;

    if (defaultlist.exists())
    {
        QSettings settingsdefault(QDir::homePath() + "/.local/share/applications/mimeapps.list", QSettings::IniFormat);
        settingsdefault.beginGroup("Added Associations");
        desktop = settingsdefault.value(mime).toString();
        settingsdefault.endGroup();
    }

    if (desktop.isEmpty() || !defaultlist.exists())
    {
        QSettings settingsdist("/usr/share/applications/mimeinfo.cache", QSettings::IniFormat);
        settingsdist.beginGroup("MIME Cache");
        desktop = settingsdist.value(mime).toString();
        settingsdist.endGroup();
    }

    QSettings desksett("/usr/share/applications/" + desktop.split(";")[0], QSettings::IniFormat);
    desksett.beginGroup("Desktop Entry");
    QString programm = desksett.value("Exec").toString().split(" ")[0];
    desksett.endGroup();

    return programm;
}

bool Mime::generatePreview(const QString &file)
{
    QPixmap pix(file);
    if (pix.size().width() > 200)
    {
        // generate random number for preview db from /dev/*random
        qint32 s;
        syscall(SYS_getrandom, &s, sizeof(qint32), 0);

        Settings settings;
        settings.setPreviewCache(file, QString::number(s));

        QFile ftw(QDir::homePath() + "/.cache/flitter/fm/" + QString::number(s) + ".png");

        ftw.open(QIODevice::WriteOnly);
        pix = pix.scaled(QSize(176, 176), Qt::KeepAspectRatio);
        pix.save(&ftw);
        ftw.close();

        return true;
    }

    return false;
}

QIcon Mime::getFolderIcon(const QString &url, const QString &file)
{
    QIcon icon;
    Settings settings;
    QMap<QString, int> folders = settings.getFolderColors();
    QList<QString> cfoldersl = {"blue", "black", "brown", "cyan", "green", "gray", "magenta", "orange", "red", "teal", "violet", "yellow"};

    if (folders[url + "/" + file] != 0)
    {
        icon = QIcon::fromTheme("folder-" + cfoldersl[folders[url + "/" + file]]);
    }
    else
    {
        icon = QIcon::fromTheme("folder");
    }

    return icon;
}

/*FFMpeg thumbnailer code was taken from here:
 * https://github.com/dirkvdb/ffmpegthumbnailer/blob/master/kffmpegthumbnailer/kffmpegthumbnailer.cpp*/
void Mime::generateVideoPreview(const QString &file)
{
    QImage *img = new QImage();

    try
    {
        std::vector<uint8_t> pixelBuffer;

        m_Thumbnailer.setThumbnailSize(176);
        m_Thumbnailer.generateThumbnail(std::string(file.toUtf8()), Png, pixelBuffer);

        img->loadFromData(&pixelBuffer.front(), pixelBuffer.size(), "PNG");

        QFileInfo fi(file);
        img->save(QDir::homePath() + "/.cache/flitter/fm/" + fi.baseName() + ".png", "PNG", 100);
    }
    catch (std::exception e){}
}

QIcon Mime::getMimeIcon(const QString &url, const QString &file)
{
    QIcon icon;

    QFileInfo fi(file);
    QString mime = QMimeDatabase().mimeTypeForFile(fi).name();

    if (mime == "image/svg+xml" || mime == "image/svg+xml-compressed")
    {
        icon = QIcon(url + "/" + file);
    }
    else if (mime.split("/")[0] == "image")
    {
        ftlip cacheicon(QDir::homePath() + "/.cache/flitter/previewdb.cfg");
        const QString &i = cacheicon.get(url + "/" + file);
        if (i.isEmpty())
        {
            if (!generatePreview(url + "/" + file))
            {
                // file less then 200px wight, do not generate anything to save memory
                // also generated preview is lower in word of quality then original
                icon = QIcon(url + "/" + file);
                return icon;
            }
        }

        icon = QIcon(QPixmap(QDir::homePath() + "/.cache/flitter/fm/" + i + ".png"));
        if (icon.isNull())
            icon = QIcon::fromTheme("image");
    }
    else if (mime.split("/")[0] == "video")
    {
        QFile cacheicon(QDir::homePath() + "/.cache/flitter/fm/" + fi.baseName() + ".png");
        if (!cacheicon.exists())
            generateVideoPreview(url + "/" + file);

        icon = QIcon(QPixmap(QDir::homePath() + "/.cache/flitter/fm/" + fi.baseName() + ".png"));
        if (icon.isNull())
            icon = QIcon::fromTheme("gnome-mime-video");
    }
    else
    {
        // all mime icons have names like mime_type-mime_name
        mime.replace("/", "-");
        icon = QIcon::fromTheme(mime, QIcon::fromTheme("stock_unknown"));
    }

    /*if (fi.isSymLink())
    {
        QPixmap base = icon.pixmap(icon.availableSizes().first()), overlay = QPixmap(":/images/link.svg");
        QPixmap result(base.width(), base.height());
        result.fill(Qt::transparent);
        QPainter painter(&result);
        painter.drawPixmap(0, 0, base);
        painter.drawPixmap(0, 0, overlay);
        painter.end();

        icon = QIcon(result);
    }*/

    return icon;
}
