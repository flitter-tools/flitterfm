#-------------------------------------------------
#
# Project created by QtCreator 2018-11-02T18:02:59
#
#-------------------------------------------------

QT       += core gui widgets

CONFIG += c++14

TARGET = FlitterFM
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

LIBS += -lffmpegthumbnailer

SOURCES += main.cpp\
        ui/mainwindow.cpp \
        ui/widgets/storagewidget.cpp \
        ui/newobjectdialog.cpp \
        ui/propertiesdialog.cpp \
        ui/appsdialog.cpp \
        ui/passworddialog.cpp \
        ui/widgets/lwfiles.cpp \
        ui/widgets/customslider.cpp \
        ui/widgets/customlwitem.cpp \
        core/mountpoints.cpp \
        core/mime.cpp \
        core/commonactions.cpp \
        utils/utils.cpp \
        utils/settings.cpp \
        third-party/ftlip/QT/ftlip.cpp

HEADERS  += ui/mainwindow.h \
        utils/utils.h \
        ui/widgets/storagewidget.h \
        ui/newobjectdialog.h \
        ui/propertiesdialog.h \
        ui/appsdialog.h \
        ui/passworddialog.h \
        ui/widgets/lwfiles.h \
        ui/widgets/customslider.h \
        ui/widgets/customlwitem.h \
        core/commonactions.h \
        core/mountpoints.h \
        core/mime.h \
        utils/settings.h \
        third-party/ftlip/QT/ftlip.h

FORMS    += ui/mainwindow.ui \
    ui/newobjectdialog.ui \
    ui/propertiesdialog.ui \
    ui/appsdialog.ui \
    ui/passworddialog.ui

RESOURCES += \
    resources.qrc
