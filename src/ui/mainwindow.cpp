#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileInfo>
#include <QProcess>
#include <QMenu>
#include <QDir>
#include <QDebug>
#include <QFileSystemWatcher>
#include <QUrl>

#include "core/mountpoints.h"
#include "core/mime.h"
#include "ui/passworddialog.h"
#include "ui/widgets/storagewidget.h"
#include "utils/settings.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    settings(new Settings),
    ui_urlbar(new QLineEdit),
    ui_sizemsg(new QLabel),
    ui_lw_iconsize(new CustomSlider),
    ui_tb(new QTabBar),
    ui_lw_files(new LWFiles),
    ui_empty(new QWidget),
    size(48),
    hidden(false)
{
    ui->setupUi(this);

    ui->toolBar->addWidget(ui_urlbar);
    ui->toolBar->addAction(ui->actionView_dual_panel);
    ui->toolBar->addAction(ui->actionView_grid_panel);
    ui->toolBar->addAction(ui->actionView_list);

    connect(ui_lw_files, &LWFiles::itemSelectionChanged, this, &MainWindow::on_lw_files_itemSelectionChanged);
    connect(ui_lw_files, &QListWidget::doubleClicked, this, &MainWindow::open);
    connect(ui_lw_files, &LWFiles::populateFinished, [&](){
        ui_urlbar->clear();
        ui_urlbar->setText(ui_lw_files->getUrl());
        this->setWindowTitle(ui_urlbar->text());
    });

    ui_tb->setTabsClosable(true);
    ui_tb->setAutoHide(true);
    ui_tb->addTab(QDir::homePath());
    ui->MainLayout->insertWidget(0, ui_tb);
    ui_tb->setTabData(0, QDir::homePath());
    ui->stw_pager->insertWidget(0, ui_lw_files);

    connect(ui_tb, &QTabBar::tabCloseRequested, this, &MainWindow::on_tab_close_triggered);
    connect(ui_tb, &QTabBar::currentChanged, [&](int index){
        ui->stw_pager->setCurrentIndex(index);
        ui_urlbar->setText(ui_tb->tabData(index).toString());
    });

    connect(ui_urlbar, &QLineEdit::textEdited, [&](const QString &url)
    {
        QUrl u(url);
        if (!u.isRelative())
        {
            if (u.scheme() == "ftp")
            {
                PasswordDialog passwd(this, url, PasswordDialog::MODE::FTP);
                passwd.exec();
            }
        }
        else
        {
            QFileInfo file(url);
            if (file.isDir() && file.exists())
            {
                LWFiles *cw = qobject_cast<LWFiles*>(ui->stw_pager->currentWidget());
                cw->populate(url, hidden);
            }
        }
    });

    /*QFileSystemWatcher watcher;
    watcher.addPath("/dev");
    connect(&watcher, &QFileSystemWatcher::directoryChanged, this, &MainWindow::populateMounts);*/

    ui->statusBar->layout()->addWidget(ui_sizemsg);

    ui_empty->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->statusBar->layout()->addWidget(ui_empty);

    connect(ui_lw_iconsize, &CustomSlider::valueChanged, [&](int value)
    {
        LWFiles *cw = static_cast<LWFiles*>(ui->stw_pager->currentWidget());
        cw->setIconSize(QSize(value, value));
        cw->setGridSize(QSize(value+(16*2), value+(16*2)));

        this->size = value+16;

        for (int i = 0; i < cw->count(); ++i)
        {
            QListWidgetItem *item = cw->item(i);
            item->setSizeHint(QSize(value+16+64, value+16));
        }

        cw->setSize(this->size);
    });

    ui_lw_iconsize->setValue(settings->loadIconSize());

    ui->statusBar->layout()->addWidget(ui_lw_iconsize);

    ui_lw_files->populate(settings->loadUrl(), hidden);
    this->populateMounts();

    this->resize(settings->loadWindowSize());
    if (settings->loadUnwrap())
        this->showMaximized();

    //ui->splitter->restoreState(settings->loadSplitter());
}

MainWindow::~MainWindow()
{
    settings->saveUrl(ui_urlbar->text());
    settings->saveIconSize(ui_lw_iconsize->value());
    settings->saveWindowSize(this->width(), this->height());
    settings->saveUnwrap(this->isMaximized());
    //settings->saveSplitter(ui->splitter->saveState());

    delete settings;
    delete ui_tb;
    delete ui_lw_files;
    delete ui_urlbar;
    delete ui_sizemsg;
    delete ui_lw_iconsize;
    delete ui;
}

void MainWindow::on_tab_close_triggered()
{
    delete ui->stw_pager->currentWidget();
    ui_tb->removeTab(ui_tb->currentIndex());
}

void MainWindow::populateMounts()
{
    ui->lw_devices->clear();

    MountPoints mount;
    QList<MountPoints::DEVICES> devices = mount.devices();

    for(const MountPoints::DEVICES &ditem : devices)
    {
        StorageWidget *sw = new StorageWidget(ui->lw_devices, ditem.name);
        QListWidgetItem *item = new QListWidgetItem(ui->lw_devices);
        item->setIcon(ditem.icon);
        item->setSizeHint(sw->sizeHint());
        item->setData(Qt::UserRole, ditem.name);
        ui->lw_devices->setItemWidget(item, sw);
    }

    QListWidgetItem *item = ui->lw_devices->item(1);
    ui->lw_devices->setFixedHeight(ui->lw_devices->visualItemRect(item).height()*ui->lw_devices->count());
}

void MainWindow::open(const QModelIndex &index)
{
    LWFiles *cw = qobject_cast<LWFiles*>(ui->stw_pager->currentWidget());

    QString slash = "";

    if (ui_urlbar->text().lastIndexOf("/") != -1)
        slash = "/";

    const QString &fileReal = ui_urlbar->text() + slash + index.data().toString();

    ui_tb->setTabData(ui->stw_pager->currentIndex(), fileReal);

    const QFileInfo fi(fileReal);

    this->setCursor(Qt::WaitCursor);

    if(fi.isDir()) // open dir
        cw->populate(fileReal, hidden);
    else if (fi.isExecutable()) // execute binary file or script
        QProcess::startDetached(fileReal);
    else if (fi.suffix() == "iso") // mount iso img file to /media using udevil
        MountPoints::mount(fileReal);
    else // start external programm associate with this file type
        QProcess::startDetached(Mime::getProgramm(fileReal) + " \"" + fileReal + "\"");

    this->setCursor(Qt::ArrowCursor);
}

void MainWindow::on_actionTop_triggered()
{
    QStringList urllist = ui_urlbar->text().split("/");
    QString newUrl = "";

    // concentrate all url and then parse it
    for (int i = 0; i < urllist.size()-1; ++i)
    {
        if (i < urllist.size()-2) // get top directory
            newUrl += urllist[i] + "/";
        else
            newUrl += urllist[i];
    }

    if (newUrl.isEmpty()) // we're in root
        newUrl = "/";

    LWFiles *cw = qobject_cast<LWFiles*>(ui->stw_pager->currentWidget());
    cw->populate(newUrl, hidden);
}

void MainWindow::on_actionHome_triggered()
{
    LWFiles *cw = qobject_cast<LWFiles*>(ui->stw_pager->currentWidget());
    cw->populate(QDir::homePath(), hidden);
}

void MainWindow::on_lw_files_itemSelectionChanged()
{
    LWFiles *cw = qobject_cast<LWFiles*>(ui->stw_pager->currentWidget());
    ui_sizemsg->setText(cw->getObjectSize(ui_urlbar->text()));
}

void MainWindow::on_actionExit_triggered()
{
    qApp->quit();
}

void MainWindow::on_actionOpen_terminal_triggered()
{
    // todo: arch doesn't have this shit, oh well
    QDir::setCurrent(ui_urlbar->text());
    QProcess::startDetached("x-terminal-emulator");
}

void MainWindow::on_lw_devices_clicked(const QModelIndex &index)
{
    LWFiles *cw = qobject_cast<LWFiles*>(ui->stw_pager->currentWidget());
    cw->populate(index.data(Qt::UserRole).toString(), hidden);
    ui->lw_devices->clearSelection();
}

void MainWindow::on_actionNew_bookmarks_triggered()
{
    QListWidgetItem *item = new QListWidgetItem;
    item->setText("Bookmark");
    item->setIcon(QIcon::fromTheme("bookmarks"));
    ui->lw_bookmarks->addItem(item);
}

void MainWindow::on_lw_bookmarks_customContextMenuRequested(const QPoint &pos)
{
    QPoint globalPos = ui->lw_bookmarks->mapToGlobal(pos);

    QMenu myMenu;
    myMenu.addAction(ui->actionNew_bookmarks);
    myMenu.addAction(ui->actionAdd_current);
    myMenu.exec(globalPos);
}

void MainWindow::on_actionAdd_current_triggered()
{
    QString text = ui_urlbar->text();
    QListWidgetItem *item = new QListWidgetItem;
    item->setText(text);
    item->setData(Qt::UserRole, text);
    item->setIcon(QIcon::fromTheme("bookmarks"));
    ui->lw_bookmarks->addItem(item);
}

void MainWindow::on_lw_bookmarks_clicked(const QModelIndex &index)
{
    ui_lw_files->populate(index.data(Qt::UserRole).toString(), hidden);
    ui->lw_bookmarks->clearSelection();
}

void MainWindow::on_actionView_list_triggered()
{
    LWFiles *cw = qobject_cast<LWFiles*>(ui->stw_pager->currentWidget());
    cw->setListMode();
}

void MainWindow::on_actionView_grid_panel_triggered()
{
    LWFiles *cw = qobject_cast<LWFiles*>(ui->stw_pager->currentWidget());
    cw->setIconMode(this->size);
}

void MainWindow::on_actionShow_hidden_triggered()
{
    LWFiles *cw = qobject_cast<LWFiles*>(ui->stw_pager->currentWidget());

    if (!hidden)
        hidden = true;
    else
        hidden = false;

    cw->populate(ui_urlbar->text(), hidden);
}

void MainWindow::on_actionNewTab_triggered()
{
    const QString &home = QDir::homePath();

    ui_tb->addTab(home);
    ui_tb->setTabData(1, home);
    LWFiles *lw_files = new LWFiles;
    ui->stw_pager->insertWidget(1, lw_files);
    lw_files->populate(home, hidden);

    ui_urlbar->setText(home);

    connect(lw_files, &LWFiles::itemSelectionChanged, this, &MainWindow::on_lw_files_itemSelectionChanged);
    connect(lw_files, &QListWidget::doubleClicked, this, &MainWindow::open);
    connect(lw_files, &LWFiles::populateFinished, [&](){
        LWFiles *cw = qobject_cast<LWFiles*>(ui->stw_pager->currentWidget());
        ui_urlbar->clear();
        ui_urlbar->setText(cw->getUrl());
        ui_tb->setTabData(1, cw->getUrl());
    });
}
