#ifndef PASSWORDDIALOG_H
#define PASSWORDDIALOG_H

#include <QDialog>

namespace Ui {
class PasswordDialog;
}

class PasswordDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit PasswordDialog(QWidget *parent = 0, const QString &who = "", int mode = 0);
        ~PasswordDialog();

        enum MODE
        {
            FTP = 0,
            SSH,
            WEBDAV
        };

    private slots:
        void on_buttonBox_accepted();

    private:
        Ui::PasswordDialog *ui;

        int mode;
        QString url;
};

#endif // PASSWORDDIALOG_H
