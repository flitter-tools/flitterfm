#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QListWidgetItem>
#include <QLabel>
#include <QSlider>
#include <QTabBar>

#include "ui/widgets/lwfiles.h"
#include "ui/widgets/customslider.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    private slots:
        void on_actionTop_triggered();
        void on_actionHome_triggered();
        void on_lw_files_itemSelectionChanged();
        void on_actionExit_triggered();
        void on_actionOpen_terminal_triggered();
        void open(const QModelIndex &index);
        void on_lw_devices_clicked(const QModelIndex &index);
        void populateMounts();
        void on_actionNew_bookmarks_triggered();
        void on_lw_bookmarks_customContextMenuRequested(const QPoint &pos);
        void on_lw_bookmarks_clicked(const QModelIndex &index);
        void on_actionAdd_current_triggered();
        void on_actionView_list_triggered();
        void on_actionView_grid_panel_triggered();
        void on_actionShow_hidden_triggered();
        void on_actionNewTab_triggered();
        void on_tab_close_triggered();

    private:
        Ui::MainWindow *ui;
        Settings *settings;

        QLineEdit *ui_urlbar;
        QLabel *ui_sizemsg;
        CustomSlider *ui_lw_iconsize;
        QTabBar *ui_tb;
        LWFiles *ui_lw_files;
        QWidget *ui_empty;

        int size;
        bool hidden;
};

#endif // MAINWINDOW_H
