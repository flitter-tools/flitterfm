#include "newobjectdialog.h"
#include "ui_newobjectdialog.h"

NewObjectDialog::NewObjectDialog(QWidget *parent, const QString &path, int mode) :
    QDialog(parent),
    ui(new Ui::NewObjectDialog)
{
    ui->setupUi(this);

    if (mode == MODE::DEFAULT)
        ui->rb_file->setChecked(true);

    if (mode == MODE::FILE)
        ui->rb_file->setChecked(true);
    else if (mode == MODE::DIR)
        ui->rb_dir->setChecked(true);
    else if (mode == MODE::LINK)
        ui->rb_link->setChecked(true);

    ui->le_path->setText(path);
}

NewObjectDialog::~NewObjectDialog()
{
    delete ui;
}

void NewObjectDialog::on_btn_cancel_clicked()
{
    this->close();
}

void NewObjectDialog::on_btn_create_clicked()
{
    if (ui->rb_dir->isChecked())
    {
        QDir().mkpath(ui->le_path->text() + "/" + ui->le_name->text());
    }
    else if (ui->rb_file->isChecked())
    {
        QFile file(ui->le_path->text() + "/" + ui->le_name->text());
        file.open(QIODevice::WriteOnly);
    }

    this->close();
}

QString NewObjectDialog::fileName()
{
    return ui->le_name->text();
}

int NewObjectDialog::mode()
{
    if (ui->rb_file->isChecked())
        return MODE::FILE;
    else if (ui->rb_dir->isChecked())
        return MODE::DIR;
    else if (ui->rb_link->isChecked())
        return MODE::LINK;

    return MODE::DEFAULT;
}
