#include "appsdialog.h"
#include "ui_appsdialog.h"

#include <QSettings>
#include <QDirIterator>
#include <QDebug>
#include <QProcess>

#include "core/mime.h"

AppsDialog::AppsDialog(QWidget *parent, const QString &file) :
    QDialog(parent),
    ui(new Ui::AppsDialog)
{
    ui->setupUi(this);

    QList<Mime::Associations> assoc = Mime::getAssociations(file);
    for (const Mime::Associations &item : assoc)
    {
        QListWidgetItem *lwitem = new QListWidgetItem(item.icon, item.name);
        ui->lw_connected->addItem(lwitem);
    }

    this->file = file;
}

AppsDialog::~AppsDialog()
{
    delete ui;
}

void AppsDialog::on_buttonBox_rejected()
{
    this->close();
}

void AppsDialog::on_btn_overview_clicked()
{

}

void AppsDialog::on_tabWidget_currentChanged(int index)
{
    if (index == 1 and ui->lw_all->count() < 1)
        fillApps();
}

void AppsDialog::fillApps()
{
    QDirIterator it("/usr/share/applications/", QStringList() << "*.desktop", QDir::Files);

    while (it.hasNext())
    {
        QSettings desktop(it.next(), QSettings::IniFormat);
        desktop.beginGroup("Desktop Entry");
        bool display = desktop.value("NoDisplay").toBool();
        QString name = desktop.value("Name").toString();
        QIcon icon = QIcon::fromTheme(desktop.value("Icon").toString());
        QString exec = desktop.value("Exec").toString();
        desktop.endGroup();

        if (display) // must be true
            continue;

        QListWidgetItem *item = new QListWidgetItem(icon, name);
        item->setData(Qt::UserRole, exec.split(" ")[0]);
        ui->lw_all->addItem(item);
    }
}

void AppsDialog::on_buttonBox_accepted()
{
    QListWidgetItem *item = ui->lw_all->currentItem();
    QProcess::startDetached(item->data(Qt::UserRole).toString() + " " + this->file);
}
