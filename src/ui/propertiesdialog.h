#ifndef PROPERTIESDIALOG_H
#define PROPERTIESDIALOG_H

#include <QDialog>

namespace Ui {
class PropertiesDialog;
}

class PropertiesDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit PropertiesDialog(QWidget *parent = 0, const QString &file = "", const QString &path = "");
        ~PropertiesDialog();

    private slots:
        void on_buttonBox_accepted();

    private:
        Ui::PropertiesDialog *ui;

        QString file;
};

#endif // PROPERTIESDIALOG_H
