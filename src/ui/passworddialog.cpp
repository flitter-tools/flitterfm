#include "passworddialog.h"
#include "ui_passworddialog.h"

#include "core/mountpoints.h"

PasswordDialog::PasswordDialog(QWidget *parent, const QString &who, int mode) :
    QDialog(parent),
    ui(new Ui::PasswordDialog)
{
    ui->setupUi(this);

    ui->l_who->setText(who);

    this->mode = mode;
    this->url = who;
}

PasswordDialog::~PasswordDialog()
{
    delete ui;
}

void PasswordDialog::on_buttonBox_accepted()
{
    if (ui->le_user->text().isEmpty())
    {
        ui->le_user->setStyleSheet("border: 1px solid #ff0000;");
        return;
    }

    if (ui->le_password->text().isEmpty())
    {
        ui->le_password->setStyleSheet("border: 1px solid #ff0000");
        return;
    }

    if (this->mode == MODE::FTP)
    {
        MountPoints::mountftp(url.remove("ftp://"), ui->le_user->text(), ui->le_password->text());
    }
    else if (this->mode == MODE::SSH)
    {
        return; // stab
    }
    else if (this->mode == MODE::WEBDAV)
    {
        return; // stab
    }

    this->close();
}
