#include "lwfiles.h"

#include <QStringList>
#include <algorithm>
#include <QCollator>
#include <QListWidgetItem>
#include <QMap>
#include <QString>
#include <QProcess>
#include <QFileInfo>
#include <QInputDialog>
#include <QDebug>

#include "core/mime.h"
#include "core/commonactions.h"
#include "utils/utils.h"
#include "ui/appsdialog.h"
#include "ui/propertiesdialog.h"
#include "ui/widgets/customlwitem.h"

LWFiles::LWFiles(QWidget *parent) : QListWidget(parent)
{
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    this->setFrameShape(QListWidget::NoFrame);
    this->setEditTriggers(QListWidget::NoEditTriggers);
    this->setSelectionMode(QAbstractItemView::ContiguousSelection);
    this->setIconSize(QSize(48, 48));
    this->setFlow(Flow::LeftToRight);
    this->setGridSize(QSize(96, 96));
    this->setViewMode(ViewMode::IconMode);
    //this->setUniformItemSizes(true);
    //this->setWordWrap(true);
    this->setSelectionRectVisible(true);
    this->setResizeMode(ResizeMode::Adjust);

    a_new = new QAction(QIcon::fromTheme("add"), "New");
    a_new->setShortcut(QKeySequence("Ctrl+N"));

    a_cut = new QAction(QIcon::fromTheme("edit-cut"), "Cut");
    a_cut->setShortcut(QKeySequence("Ctrl+X"));

    a_copy = new QAction(QIcon::fromTheme("edit-copy"), "Copy");
    a_copy->setShortcut(QKeySequence("Ctrl+C"));

    a_paste = new QAction(QIcon::fromTheme("edit-paste"), "Paste");
    a_paste->setShortcut(QKeySequence("Ctrl+V"));

    a_rename = new QAction(QIcon::fromTheme("edit"), "Rename");
    a_rename->setShortcut(QKeySequence("F2"));

    a_delete = new QAction(QIcon::fromTheme("edit-delete"), "Delete");
    a_delete->setShortcut(QKeySequence("Del"));

    a_properties = new QAction(QIcon::fromTheme("settings"), "Properties");
    a_properties->setShortcut(QKeySequence("Alt+Enter"));

    a_openwith = new QAction(QIcon::fromTheme("document-open"), "Open with...");

    connect(this, &LWFiles::customContextMenuRequested, this, &LWFiles::showMenu);
    connect(a_new, &QAction::triggered, this, &LWFiles::onNewObjectTriggered);
    connect(a_rename, &QAction::triggered, this, &LWFiles::onRenameTriggered);
    connect(a_delete, &QAction::triggered, this, &LWFiles::onRemoveTriggered);
    connect(a_openwith, &QAction::triggered, this, &LWFiles::onOpenWithTriggered);
    connect(a_properties, &QAction::triggered, this, &LWFiles::onPropertiesTriggered);
}

LWFiles::~LWFiles()
{
    delete a_new;
    delete a_cut;
    delete a_copy;
    delete a_paste;
    delete a_rename;
    delete a_properties;
    delete a_openwith;
}

void LWFiles::setIconMode(const int &size)
{
    this->setViewMode(QListWidget::IconMode);
    this->setIconSize(QSize(48, 48));
    this->setGridSize(QSize(48+(16*2), 48+(16*2)));
    this->setResizeMode(QListWidget::Adjust);
    this->setWordWrap(true);
    this->setFlow(QListWidget::LeftToRight);

    for (int i = 0; i < this->count(); ++i)
    {
        QListWidgetItem *item = this->item(i);
        item->setTextAlignment(Qt::AlignHCenter | Qt::AlignBottom);
        item->setSizeHint(QSize(size, size));
    }
}

void LWFiles::setListMode()
{
    this->setViewMode(QListWidget::ListMode);
    this->setIconSize(QSize(-1, -1));
    this->setGridSize(QSize(-1, -1));
    this->setResizeMode(QListWidget::Fixed);
    this->setWordWrap(false);
    this->setFlow(QListWidget::TopToBottom);

    for (int i = 0; i < this->count(); ++i)
    {
        QListWidgetItem *item = this->item(i);
        item->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
        item->setSizeHint(QSize(32, 32));
    }
}

void LWFiles::populate(const QString &url, const bool &hidden)
{
    this->clear();

    QDir sysnet(url);
    QStringList dirs;
    QStringList files;
    if (hidden)
    {
        dirs = sysnet.entryList(QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Hidden);
        files = sysnet.entryList(QDir::NoDotAndDotDot | QDir::Files | QDir::Hidden);
    }
    else
    {
        dirs = sysnet.entryList(QDir::NoDotAndDotDot | QDir::AllDirs);
        files = sysnet.entryList(QDir::NoDotAndDotDot | QDir::Files);
    }

    // sort file by name
    // all dot files will be first
    QCollator collator;
    collator.setNumericMode(true);

    std::sort(dirs.begin(), dirs.end(), collator);
    std::sort(files.begin(), files.end(), collator);

    bool isHome = false;
    if (QDir::homePath() == url)
        isHome = true;

    Mime mime;

    for (const QString &item : dirs)
    {
        QListWidgetItem *lwitem = new QListWidgetItem(item);
        lwitem->setSizeHint(QSize(this->size, this->size));
        lwitem->setTextAlignment(Qt::AlignHCenter | Qt::AlignBottom);
        lwitem->setIcon(mime.getFolderIcon(url, item));

        if (isHome)
        {
            QMap<QString, QString> dirs = mime.getXDGDirs();

            if (dirs["XDG_DOCUMENTS_DIR"] == QDir::homePath() + "/" + item)
                lwitem->setIcon(QIcon::fromTheme("folder-documents"));
            else if (dirs["XDG_DOWNLOAD_DIR"] == QDir::homePath() + "/" + item)
                lwitem->setIcon(QIcon::fromTheme("folder-downloads"));
            else if (dirs["XDG_PICTURES_DIR"] == QDir::homePath() + "/" + item)
                lwitem->setIcon(QIcon::fromTheme("folder-images"));
        }

        this->addItem(lwitem);
    }

    for (const QString &item : files)
    {
        QListWidgetItem *lwitem = new QListWidgetItem(item);
        lwitem->setSizeHint(QSize(this->size, this->size));
        lwitem->setTextAlignment(Qt::AlignHCenter | Qt::AlignBottom);

        lwitem->setIcon(mime.getMimeIcon(url, item));

        this->addItem(lwitem);
    }

    this->url = url;
    emit populateFinished();
}

void LWFiles::setSize(const int &size)
{
    this->size = size;
}

QString LWFiles::getUrl() const
{
    return this->url;
}

void LWFiles::onActionMenu(QAction *action)
{
    QListWidgetItem *item = this->currentItem();
    QProcess::startDetached(action->data().toString() + " \"" + this->url + "/" + item->text() + "\"");
}

void LWFiles::onFolderChangeMenu(QAction *action)
{
    QListWidgetItem *item = this->currentItem();
    QFileInfo fi(this->url + "/" + item->text());

    if (fi.isDir())
    {
        Settings settings;
        settings.setFolderColor(this->url + "/" + item->text(), action->data().toInt());
        item->setIcon(action->icon());
    }
}

void LWFiles::showMenu(const QPoint &pos)
{
    QPoint globalPos = this->mapToGlobal(pos);

    QMenu menu;
    QMenu *menuAssociations = menu.addMenu(QIcon::fromTheme("document-open-symbolic"), "Open");
    menu.addAction(a_new);
    menu.addSeparator();
    menu.addAction(a_cut);
    menu.addAction(a_copy);
    menu.addAction(a_paste);
    menu.addAction(a_rename);
    menu.addAction(a_delete);
    menu.addSeparator();
    QMenu *menuView = menu.addMenu(QIcon::fromTheme("settings"), "View");
    menu.addAction(a_properties);

    QListWidgetItem *pointedItem = this->itemAt(pos);

    bool disabled = false;
    if (!pointedItem)
    {
        disabled = true;
    }
    else
    {
        QList<QString> folders = {"blue", "black", "brown", "cyan", "green", "gray", "magenta", "orange", "red", "teal", "violet", "yellow"};

        connect(menuView, &QMenu::triggered, this, &LWFiles::onFolderChangeMenu);

        for (int i = 0; i < folders.size(); ++i)
        {
            QAction *action = new QAction(QIcon::fromTheme("folder-" + folders[i]), "Set folder " + folders[i]);
            action->setData(i);
            menuView->addAction(action);
        }

        connect(menuAssociations, &QMenu::triggered, this, &LWFiles::onActionMenu);

        QList<Mime::Associations> assoc = Mime::getAssociations(pointedItem->text());
        for (const Mime::Associations &item : assoc)
        {
            QAction *action = new QAction(item.icon, item.name);
            action->setData(item.exec);
            menuAssociations->addAction(action);
        }

        menuAssociations->addSeparator();
        menuAssociations->addAction(a_openwith);
    }

    menuAssociations->setDisabled(disabled);
    menuView->setDisabled(disabled);

    a_cut->setDisabled(disabled);
    a_copy->setDisabled(disabled);
    a_paste->setDisabled(disabled);
    a_rename->setDisabled(disabled);
    a_delete->setDisabled(disabled);

    menu.exec(globalPos);
}

void LWFiles::onNewObjectTriggered()
{
    if (CommonActions::newObj(this->url, -1))
        this->populate(this->url, this->hidden);
}

void LWFiles::onRenameTriggered()
{
    QListWidgetItem *item = this->currentItem();

    if (item)
    {
        QString *name = new QString;
        if (CommonActions::rename(this->url, item->text(), *name))
            item->setText(*name);

        delete name;
    }
}

void LWFiles::onRemoveTriggered()
{
    QList<QListWidgetItem*> items = this->selectedItems();
    QStringList list;

    for (QListWidgetItem *item : items)
        list.push_back(this->url + "/" + item->text());

    if (CommonActions::remove(list))
        this->populate(this->url, this->hidden);
}

void LWFiles::onOpenWithTriggered()
{
    QListWidgetItem *item = this->currentItem();
    if (item)
    {
        AppsDialog dial(this, this->url + "/" + item->text());
        dial.exec();
    }
}

void LWFiles::onPropertiesTriggered()
{
    QListWidgetItem *ret = this->currentItem();
    //QList<QListWidgetItem*> items = this->selectedItems();
    //QStringList ret;

    //for (const QListWidgetItem *item : items)
    //    ret.push_back(item->text());

    PropertiesDialog dial(this, ret->text(), this->url);
    dial.exec();
}

QString LWFiles::getObjectSize(const QString &url) const
{
    QList<QListWidgetItem*> selectedItems = this->selectedItems();

    QString sizemsg = QString();

    if (selectedItems.count() > 0)
    {
        sizemsg += QString::number(selectedItems.count()) + " / " + QString::number(this->count()) + " objects";

        qint64 fsize = 0;

        for (const QListWidgetItem *item : selectedItems)
        {
            QFileInfo fi(url + "/" + item->text());
            fsize += fi.size();
        }

        sizemsg += " (" + utils::convertSize(fsize)[1] + utils::convertSize(fsize)[0] + ")";
    }
    else
    {
        sizemsg += QString::number(this->count()) + " objects";
    }

    return sizemsg;
}
