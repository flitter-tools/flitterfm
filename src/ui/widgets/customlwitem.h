#ifndef CUSTOMLWITEM_H
#define CUSTOMLWITEM_H

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QIcon>
#include <QString>

class CustomLWItem : public QWidget
{
    public:
        explicit CustomLWItem(QWidget *parent = 0);
        ~CustomLWItem();

        void setIcon(const QIcon &icon);
        QIcon getIcon() const;

        void setValue(const QString &value);
        QString getValue() const;

    private:
        QLabel *icon;
        QLabel *label;
        QVBoxLayout *layout;
};

#endif // CUSTOMLWITEM_H
