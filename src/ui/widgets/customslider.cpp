#include "customslider.h"

#include <QHBoxLayout>

CustomSlider::CustomSlider(QWidget *parent) : QWidget(parent)
{
    slider = new QSlider;
    label = new QLabel;

    slider->setOrientation(Qt::Horizontal);
    slider->setMinimum(48);
    slider->setMaximum(176);
    slider->setPageStep(16);
    slider->setSingleStep(16);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(slider);
    layout->addWidget(label);

    connect(slider, &QSlider::valueChanged, [&](int value){
        label->setText(QString::number(value));
        slider->setToolTip(QString::number(slider->value()));
        emit valueChanged(slider->value());
    });

    this->setLayout(layout);
}

CustomSlider::~CustomSlider()
{
    delete slider;
    delete label;
}

void CustomSlider::setValue(const int &value)
{
    slider->setValue(value);
}

int CustomSlider::value() const
{
    return slider->value();
}
