#ifndef LWFILES_H
#define LWFILES_H

#include <QWidget>
#include <QObject>
#include <QListWidget>
#include <QPoint>
#include <QMenu>
#include <QAction>
#include <QDir>

#include "utils/settings.h"

class LWFiles : public QListWidget
{
    Q_OBJECT

    public:
        explicit LWFiles(QWidget *parent = nullptr);
        virtual ~LWFiles();

        void setIconMode(const int &size);
        void setListMode();
        void populate(const QString &url, const bool &hidden);
        void setSize(const int &size);
        QString getUrl() const;
        QString getObjectSize(const QString &url) const;

    private slots:
        void showMenu(const QPoint &pos);
        void onActionMenu(QAction *action);
        void onFolderChangeMenu(QAction *action);
        void onNewObjectTriggered();
        void onRenameTriggered();
        void onRemoveTriggered();
        void onOpenWithTriggered();
        void onPropertiesTriggered();

    signals:
        void populateFinished();

    private:
        int size = 64;
        QString url;
        bool hidden = false;

        QAction *a_new;
        QAction *a_cut;
        QAction *a_copy;
        QAction *a_paste;
        QAction *a_rename;
        QAction *a_delete;
        QAction *a_properties;
        QAction *a_openwith;
};

#endif // LWFILES_H
