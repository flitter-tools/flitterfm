#ifndef CUSTOMSLIDER_H
#define CUSTOMSLIDER_H

#include <QWidget>
#include <QLabel>
#include <QSlider>

class CustomSlider : public QWidget
{
    Q_OBJECT

    public:
        explicit CustomSlider(QWidget *parent = 0);
        ~CustomSlider();

        void setValue(const int &value);
        int value() const;

    signals:
        void valueChanged(const int &value);

    private:
        QSlider *slider;
        QLabel *label;
};

#endif // CUSTOMSLIDER_H
