#include "customlwitem.h"

#include <QPixmap>
#include <QDebug>
#include <QFontMetrics>

CustomLWItem::CustomLWItem(QWidget *parent) : QWidget(parent)
{
    layout = new QVBoxLayout;
    icon = new QLabel;
    label = new QLabel;

    icon->setAlignment(Qt::AlignCenter);
    label->setAlignment(Qt::AlignCenter);
    label->setWordWrap(true);

    icon->setMinimumSize(48, 48);
    label->setMinimumSize(64, 64);
    label->setWordWrap(true);

    layout->addWidget(icon);
    layout->addWidget(label);

    this->setLayout(layout);
}

CustomLWItem::~CustomLWItem()
{
    delete icon;
    delete label;
    delete layout;
}

QIcon CustomLWItem::getIcon() const
{
    return QIcon();
}

QString CustomLWItem::getValue() const
{
    return label->text();
}

void CustomLWItem::setIcon(const QIcon &wicon)
{
    QPixmap pix = wicon.pixmap(48, 48);
    icon->setPixmap(pix);
}

void CustomLWItem::setValue(const QString &value)
{
    QFontMetrics metric(label->font());
    QString elide = metric.elidedText(value, Qt::ElideNone, label->height());
    label->setText(elide);
}
