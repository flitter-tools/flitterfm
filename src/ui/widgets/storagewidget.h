#ifndef STORAGEWIDGET_H
#define STORAGEWIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QAction>
#include <QPoint>
#include <QMenu>

class StorageWidget : public QWidget
{
    Q_OBJECT

    public:
        explicit StorageWidget(QWidget *parent = 0, const QString &storagePath = "");
        virtual ~StorageWidget();

        QString path() const;

    private slots:
        void showMenu(const QPoint &pos);
        void deviceProperties(const QString &device);

    private:
        QVBoxLayout *layout;
        QLabel *storage;
        QLabel *size;
        QLabel *sizeavail;

        QMenu *menu;
        QAction *a_umount;
        QAction *a_deviceproperties;
};

#endif // STORAGEWIDGET_H
