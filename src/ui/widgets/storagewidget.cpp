#include "storagewidget.h"

#include <QStorageInfo>
#include <QPainter>
#include <QRectF>
#include <QMessageBox>

#include "utils/utils.h"
#include "core/mountpoints.h"

StorageWidget::StorageWidget(QWidget *parent, const QString &storagePath) : QWidget(parent)
{
    QStorageInfo storageInfo(storagePath);

    this->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, &QWidget::customContextMenuRequested, this, &StorageWidget::showMenu);

    menu = new QMenu;

    a_umount = new QAction(QIcon::fromTheme("gtk-remove"), "Umount");
    a_deviceproperties = new QAction(QIcon::fromTheme("dialog-information"), "Device properties");

    a_umount->setData(storagePath);
    a_deviceproperties->setData(storagePath);

    connect(menu, &QMenu::triggered, [&](QAction *a){
        if (a->text() == "Umount")
            MountPoints::umount(a->data().toString());
        else
            this->deviceProperties(a->data().toString());
    });

    menu->addAction(this->a_umount);
    menu->addAction(this->a_deviceproperties);

    layout = new QVBoxLayout;

    storage = new QLabel;
    storage->setText(storagePath);

    QList<QString> btl = utils::convertSize(storageInfo.bytesTotal());
    QList<QString> bal = utils::convertSize(storageInfo.bytesAvailable());

    double bt = btl[1].toDouble();
    double ba = bal[1].toDouble();

    size = new QLabel;
    size->setGeometry(0, 0, 100, 5);

    QPixmap pm(100, 5);
    pm.fill(Qt::transparent);

    QPainter p(&pm);
    p.setRenderHint(QPainter::Antialiasing);

    QBrush brushBack(Qt::white);
    p.setBrush(brushBack);
    p.drawRect(0, 0, 100, 5);

    QBrush brush("#574f4a");
    p.setBrush(brush);

    if (bt > 0)
        p.drawRect(0, 0, int(bt - ba)*100/int(bt), 5);
    else
        p.drawRect(0, 0, 1, 3);

    size->setPixmap(pm);

    sizeavail = new QLabel;
    sizeavail->setText("(" + bal[1] + bal[0] + " / " + btl[1] + btl[0] + ")");
    sizeavail->setStyleSheet("font-size: 10px; color: #8C8C8C;");

    layout->addWidget(storage, 0, nullptr);
    layout->addWidget(size, 0, nullptr);
    layout->addWidget(sizeavail, 0, nullptr);

    this->setCursor(Qt::PointingHandCursor);

    this->setLayout(layout);
}

StorageWidget::~StorageWidget()
{
    delete storage;
    delete size;
    delete sizeavail;
    delete layout;
}

QString StorageWidget::path() const
{
    return storage->text();
}

void StorageWidget::showMenu(const QPoint &pos)
{
    QPoint globalPos = this->mapToGlobal(pos);
    menu->exec(globalPos);
}

void StorageWidget::deviceProperties(const QString &device)
{
    QMessageBox *mb = new QMessageBox(this);
    mb->setModal(true);
    mb->setWindowTitle(device);
    mb->setText(MountPoints::deviceProperties(device));
    mb->setStandardButtons(QMessageBox::Ok);
    mb->exec();
}
