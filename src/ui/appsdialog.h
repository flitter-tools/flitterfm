#ifndef APPSDIALOG_H
#define APPSDIALOG_H

#include <QDialog>

namespace Ui {
class AppsDialog;
}

class AppsDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit AppsDialog(QWidget *parent = 0, const QString &file = "");
        ~AppsDialog();

    private slots:
        void on_buttonBox_rejected();
        void on_btn_overview_clicked();
        void on_tabWidget_currentChanged(int index);

        void on_buttonBox_accepted();

private:
        Ui::AppsDialog *ui;

        void fillApps();

        QString file;
};

#endif // APPSDIALOG_H
