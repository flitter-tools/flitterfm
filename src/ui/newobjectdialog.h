#ifndef NEWOBJECTDIALOG_H
#define NEWOBJECTDIALOG_H

#include <QDialog>
#include <QDir>

namespace Ui {
class NewObjectDialog;
}

class NewObjectDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit NewObjectDialog(QWidget *parent = 0, const QString &path = QDir::homePath(), int mode = -1);
        ~NewObjectDialog();

        enum MODE
        {
            DEFAULT = -1,
            FILE = 0,
            DIR,
            LINK
        };

        QString fileName();
        int mode();

    private slots:
        void on_btn_cancel_clicked();
        void on_btn_create_clicked();

    private:
        Ui::NewObjectDialog *ui;
};

#endif // NEWOBJECTDIALOG_H
