#include "propertiesdialog.h"
#include "ui_propertiesdialog.h"

#include <QDir>
#include <QFile>
#include <QMimeDatabase>
#include <QFileInfo>
#include <QDateTime>

#include "utils/utils.h"
#include "utils/settings.h"
#include "core/mime.h"

PropertiesDialog::PropertiesDialog(QWidget *parent, const QString &file, const QString &path) :
    QDialog(parent),
    ui(new Ui::PropertiesDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Properties of " + file);

    ui->le_filename->setText(file);
    ui->le_path->setText(path);

    QFileInfo fi(path + "/" + file);

    if (fi.isDir())
        ui->l_objtype->setText("folder\ninode/directory");
    else
        ui->l_objtype->setText(QMimeDatabase().mimeTypeForFile(path + "/" + file).name());

    QList<Mime::Associations> assoc = Mime::getAssociations(path + "/" + file);
    for (const Mime::Associations &a : assoc)
        ui->comb_openwith->addItem(a.icon, a.name, a.desktop);

    ui->l_sizetotal->setText(utils::getFileSizeString(path + "/" + file) + " (" + QString::number(fi.size()) + " bytes)");
    ui->l_drivesize->setText("???");
    ui->l_count->setText("1");

    ui->le_changetimestamp->setText(fi.lastModified().toString());
    ui->le_accesstimestamp->setText(fi.lastRead().toString());

    ui->le_owner->setText(fi.owner());
    ui->le_group->setText(fi.group());

    this->file = file;
}

PropertiesDialog::~PropertiesDialog()
{
    delete ui;
}

void PropertiesDialog::on_buttonBox_accepted()
{
    if (ui->comb_openwith->currentIndex() > 0)
    {
        Settings settings;
        settings.setMimeCache(this->file, ui->comb_openwith->currentData().toString());
    }
}
